import { BsFacebook, BsTwitter, BsInstagram, BsHeart, BsCart } from 'react-icons/bs';
import React from 'react';
import { Link } from 'react-router-dom';


export default function Footer() {
    return (
        <>
        <footer className="footer p-4">
            <div className='container-xxl'>
                <div className="row">
                    <div className="col-4 d-flex flex-column">
                    <h3 className="mb-5">Contact us</h3>
                    <p className="mb-3"><b>ADDRESS:</b> Manila, Philippines</p>
                    <p className="mb-3"><b>PHONE:</b> <a href="tel:+649685934336" className="contact-link">
                    09685934336
                    </a></p>
                    <p className="mb-5"><b>OPEN HOURS:</b> From 8AM-5PM</p>
                    <p className='mb-3'><b>FOLLOW US</b></p>
                    <div className="col-3 d-flex justify=content-around">
                        <a href="https://facebook.com/justforkicks" target="_blank" className="icon">
                        <BsFacebook size={25} />
                        </a>
                        <a href="https://instagram.com/justforkicks" target="_blank" className="icon">
                        <BsInstagram size={25} />
                        </a>
                        <a href="https://twitter.com/justforkicks" target="_blank" className="icon">
                        <BsTwitter size={25} />
                        </a>
                    </div>
                    </div>
                    <div className='col-2'>
                        <h3 className="mb-5">About</h3>
                        <div className="footer-details d-flex flex-column">
                        <Link className="mb-3 contact-link">About Us</Link>
                        <Link className="mb-3 contact-link">Delivery</Link>
                        <Link className="mb-3 contact-link">Private Policy</Link>
                        <Link className="mb-3 contact-link">Fee Policy</Link>
                        <Link className="mb-3 contact-link">Terms & Conditions</Link>
                        </div>
                    </div>
                    <div className='col-2'>
                        <h3 className="mb-5">Account</h3>
                        <div className="footer-details d-flex flex-column">
                        <Link className="mb-3 contact-link">Profile</Link>
                        <Link className="mb-3 contact-link">View Cart</Link>
                        <Link className="mb-3 contact-link">My Orders</Link>
                        <Link className="mb-3 contact-link">My Wishlist</Link>
                        <Link className="mb-3 contact-link">Help</Link>
                        </div>
                    </div>
                    
                    <hr className='mt-4 ' />
                    <div className='row'>
                        <div className='col-8'> 
                            <p>&copy; Developed by Christian Ting 2023</p>
                        </div>
                        <div className='col-4 d-flex justify-content-around '>
                            <Link className='contact-link'>Guides</Link>
                            <Link className='contact-link'>Terms of Sale</Link>
                            <Link className='contact-link'>Terms of Use</Link>
                           
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        </>

    )
}