import React, { useContext, useState, useEffect } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import { MdGridView } from 'react-icons/md';
import { BsPeople } from 'react-icons/bs';
import { MdOutlineReceiptLong, MdOutlineInventory } from 'react-icons/md';
import { GrAdd } from 'react-icons/gr';
import Button from 'react-bootstrap/Button';
import Products from '../pages/Products';
import AddProduct from '../pages/AddProduct';
import Orders from '../pages/Orders';

import Swal from 'sweetalert2';



export default function Dashboard() {
  const { user } = useContext(UserContext);
  const [activeLink, setActiveLink] = useState('dashboard');
  const [products, setProducts] = useState([]);
  const [addProduct, setAddProduct] = useState([]);
  const [orders, setOrders] = useState([]);
 
  const [userId, setUserId] = useState('');
  const [message, setMessage] = useState('');

  const handleNavClick = (section) => {
    setActiveLink(section);
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const handleUserIdChange = (event) => {
    setUserId(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.text())
      .then((data) => {
        if (data === 'Updated Admin Status') {
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'User has been set as admin!',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'User is already an admin.',
          });
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };


  return (
    <div className="container1">
      <aside>
        <div className="button-container1 btn">
          <Button className="custom-button btn" as={NavLink} to="/logout">
            Logout
          </Button>
        </div>

        <div className="sidebar">
          <NavLink
            href="#dashboard"
            className={`sidebar-link ${activeLink === 'dashboard' ? 'active' : ''}`}
            onClick={() => handleNavClick('dashboard')}
          >
            <span className="icons">
              <MdGridView />
              <h6>Dashboard</h6>
            </span>
          </NavLink>
         
          <NavLink
            href="#orders"
            className={`sidebar-link ${activeLink === 'orders' ? 'active' : ''}`}
            onClick={() => handleNavClick('orders')}
          >
            <span className="icons">
              <MdOutlineReceiptLong />
              <h6>Orders</h6>
            </span>
          </NavLink>
          <NavLink
            href="#products"
            className={`sidebar-link ${activeLink === 'products' ? 'active' : ''}`}
            onClick={() => handleNavClick('products')}
          >
            <span className="icons">
              <MdOutlineInventory />
              <h6>Products</h6>
            </span>
          </NavLink>
          
        </div>
      </aside>


      <main>
        <Container fluid>
        {activeLink === 'dashboard' && (
            <div style={{ display: activeLink === 'dashboard' ? 'block' : 'none' }}>
            <h2>Dashboard</h2>
            <div className="set-admin-container">
          
              <form className="admin-form" onSubmit={handleSubmit}>
                
                  
                  <input
                    type="text"
                    value={userId}
                    placeholder='User ID'
                    onChange={handleUserIdChange}
                    className="admin-input"
                  />
                
                <button type="submit" className="admin-button">Set as Admin</button>
              </form>
              {message && <p className="admin-message">{message}</p>}
            </div>

            <div className="row mt-5">
              <div className="col-xl-4 col-md-7">
                <div className="card border-left-primary shadow h-100 py-2">
                  <div className="card-body">
                    <div className="align-items-center">
                      <div className="mr-2">
                        <div className="text-center font-weight-bold text-primary text-uppercase mb-1 mt-2">
                          
                        </div>
                        <div className="h5 mb-0 font-weight-bold text-gray-800 text-center">
                          
                        </div>
                      </div>
                      <div>
                        <i className="fas fa-calendar fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-4 col-md-7">
                <div className="card border-left-primary shadow h-100 py-2">
                  <div className="card-body">
                    <div className="align-items-center">
                      <div className="mr-2">
                        <div className="text-center font-weight-bold text-primary text-uppercase mb-1 mt-2">
                          
                        </div>
                        <div className="h5 mb-0 font-weight-bold text-gray-800 text-center">
                          
                        </div>
                      </div>
                      <div>
                        <i className="fas fa-calendar fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-4 col-md-7">
                <div className="card border-left-primary shadow h-100 py-2">
                  <div className="card-body">
                    <div className="align-items-center">
                      <div className="mr-2">
                        <div className="text-center font-weight-bold text-primary text-uppercase mb-1 mt-2">
                      
                        </div>
                        <div className="h5 mb-0 font-weight-bold text-gray-800 text-center">
                        
                        </div>
                      </div>
                      <div>
                        <i className="fas fa-calendar fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-8 col-lg-7 mt-4">
                <div className="card border-left-primary shadow h-100 py-2">
                  <div className="card-header py-3 align-items-center">
                    <h6 className="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                  </div>
                  <div className="card-body">
                    <div className="chart-area">
                      <canvas id="myAreaChart"></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-4 col-lg-5 mt-4">
                <div className="card border-left-primary shadow h-100 py-2">
                  <div className="card-header py-3 align-items-center">
                    <h6 className="m-0 font-weight-bold text-primary">Revenue Sources</h6>
                  </div>
                  <div className="card-body">
                    <div className="chart-area">
                      <canvas id="myPieChart"></canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          )}
      
            {activeLink === 'products' && (
            <div style={{ display: activeLink === 'products' ? 'block' : 'none' }}>
              <h2>Products</h2>
              <Products products={products} setProducts={setProducts} />
            </div>
          )}

            {activeLink === 'addProduct' && (
            <div style={{ display: activeLink === 'addProduct' ? 'block' : 'none' }}>
              <h2>Products</h2>
              <AddProduct addProduct={addProduct} setAddProduct={setAddProduct} />
            </div>
          )}

            {activeLink === 'orders' && (
            <div style={{ display: activeLink === 'orders' ? 'block' : 'none' }}>
              <h2>Orders</h2>
              <Orders orders={orders} setOrders={setOrders} />
            </div>
          )}
           
          
        </Container>
      </main>
    </div>
  );
}
