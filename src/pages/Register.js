import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  const [isPassed2, setIsPassed2] = useState(true);
  const [isDisabled, setIsDisabled] = useState(true);
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  useEffect(() => {
    if (mobileNumber.length !== 11) {
      setIsPassed2(false);
    } else {
      setIsPassed2(true);
    }
  }, [mobileNumber]);

  useEffect(() => {
    if (
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2 &&
      firstName !== '' &&
      lastName !== '' &&
      mobileNumber !== ''
      
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2]);

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email
      }),
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Duplicate Email Found',
            icon: 'error',
            text: 'Please provide a different email address',
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNumber,
              password: password2,
            }),
          })
            .then(response => response.json())
            .then(data => {
              if (data === true) {
                Swal.fire({
                  title: 'Registration successful!',
                  icon: 'success',
                  text: 'Thank you for registering!',
                })
                .then(() => {
                  navigate('/login');
                });
              } else {
                Swal.fire({
                  title: 'Registration Failed',
                  icon: 'error',
                  text: 'Failed to register. Please try again.',
                });
              }
            })
            .catch(error => {
              console.log(error);
              Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'An error occurred. Please try again later.',
              });
            });
        }
      })
      .catch(error => {
        console.log(error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred. Please try again later.',
        });
      });
  }

  useEffect(() => {
    if (password1 === password2) {
      setIsPasswordMatch(true);
    } else {
      setIsPasswordMatch(false);
    }
  }, [password1, password2]);

  return (
    user.id === null || user.id === undefined ? (
      <Row>
        <Col className="col-6 mx-auto d-flex align-items-center justify-content-center login-form">
          <h1 className="col-8 p-4">Register</h1>
          <Form onSubmit={registerUser}>
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Control
                type="text"
                placeholder="Enter your first name"
                className="custom-input"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Control
                type="text"
                placeholder="Enter your last name"
                className="custom-input"
                value={lastName}
                onChange={(event) => setLastName(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Enter email"
                className="custom-input"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicMobileNumber">
              <Form.Control
                type="tel"
                placeholder="Enter your mobile number"
                className="custom-input"
                value={mobileNumber}
                onChange={(event) => {
                  const inputValue = event.target.value;
                  const numericValue = inputValue.replace(/\D/g, '');

                  if (numericValue.length <= 11) {
                    setMobileNumber(numericValue);
                  }
                }}
              />
              <Form.Text className="text-muted" >
                The mobile number should have at least 11 digits!
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword1">
              <Form.Control
                type="password"
                placeholder="Password"
                className="custom-input"
                value={password1}
                onChange={(event) => setPassword1(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword2">
              <Form.Control
                type="password"
                placeholder="Retype your nominated password"
                className="custom-input"
                value={password2}
                onChange={(event) => setPassword2(event.target.value)}
              />

              <Form.Text className="text-muted" hidden={isPasswordMatch}>
                The password does not match!
              </Form.Text>
            </Form.Group>

            <Button type="submit" disabled={isDisabled} className="login-button mb-5">
              Sign up
            </Button>
          </Form>
        </Col>
      </Row>
    ) : (
      <Navigate to="/*" />
    )
  );
}
