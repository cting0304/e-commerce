import React, { useEffect, useState } from 'react';
import '../App.css';
import { FiEdit, FiSearch, FiTrash2 } from 'react-icons/fi';
import Swal from 'sweetalert2';
import { Modal, Button } from 'react-bootstrap'; // Updated import
import PropTypes from 'prop-types';



export default function ProductPage() {
  const [products, setProducts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchedProduct, setSearchedProduct] = useState(null);
  const [editingProduct, setEditingProduct] = useState(null);
  const [showEditModal, setShowEditModal] = useState(false);
  const [searchFilter, setSearchFilter] = useState('all');
  const [modalOpen, setModalOpen] = useState(false);
  const [product, setProduct] = useState({
    name: '',
    description: '',
    price: 0,
    imageURL: ''
  });
 

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleArchive = (productId) => {
    Swal.fire({
      title: 'Archive Product',
      text: 'Are you sure you want to archive this product?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Archive',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#6c757d',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const updatedProducts = products.map((product) =>
            product._id === productId ? { ...product, isActive: false } : product
          );
          setProducts(updatedProducts);

          if (searchedProduct && searchedProduct._id === productId) {
            setSearchedProduct((prevProduct) => ({ ...prevProduct, isActive: false }));
          }

          await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          });

          Swal.fire('Success', 'Product archived successfully', 'success');
          fetchProducts();
        } catch (error) {
          console.error(error);
        }
      }
    });
  };

  const handleActivate = (productId) => {
    Swal.fire({
      title: 'Activate Product',
      text: 'Are you sure you want to activate this product?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Activate',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#28a745',
      cancelButtonColor: '#6c757d',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const updatedProducts = products.map((product) =>
            product._id === productId ? { ...product, isActive: true } : product
          );
          setProducts(updatedProducts);

          if (searchedProduct && searchedProduct._id === productId) {
            setSearchedProduct((prevProduct) => ({ ...prevProduct, isActive: true }));
          }

          const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          });

          const data = await response.json();

          if (data.success) {
            console.log(`Product with ID ${productId} activated successfully.`);
          }

          Swal.fire({
            title: 'Success',
            text: 'Product activated successfully',
            icon: 'success',
          });

          fetchProducts();
        } catch (error) {
          console.error(error);
          Swal.fire({
            title: 'Error',
            text: 'Failed to activate product',
            icon: 'error',
          });
        }
      }
    });
  };

  const handleEdit = (productId) => {
    const productToEdit = products.find((product) => product._id === productId);
    setEditingProduct(productToEdit);
    setShowEditModal(true);
  };

  const handleUpdate = () => {
    try {
      fetch(`${process.env.REACT_APP_API_URL}/products/${editingProduct._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(editingProduct),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Failed to update product');
          }
        })
        .then((data) => {
          console.log(data);
          fetchProducts();
          setShowEditModal(false);
          Swal.fire('Success', 'Product updated successfully', 'success');
        })
        .catch((error) => {
          console.error(error);
          Swal.fire('Error', error.message, 'error');
        });
    } catch (error) {
      console.error(error);
      Swal.fire('Error', 'Failed to update product', 'error');
    }
  };

  const handleCancelEdit = () => {
    setEditingProduct(null);
    setShowEditModal(false);
  };

  const handleSearch = async () => {
    try {
      if (searchFilter === 'id') {
        if (searchTerm.trim() === '') {
          Swal.fire('Error', 'Please enter a product ID', 'error');
          return;
        }
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${searchTerm}`);
        const data = await response.json();

        if (data.error) {
          Swal.fire('Error', data.error, 'error');
        } else {
          setSearchedProduct(data);
        }
      } else if (searchFilter === 'active') {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products?filter=active`);
        const data = await response.json();
        setProducts(data);
        setSearchedProduct(null);
        setSearchTerm('');
      } else {
        fetchProducts();
        setSearchedProduct(null);
        setSearchTerm('');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchAll = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products`);
      const data = await response.json();
      setProducts(data);
      setSearchedProduct(null);
      setSearchTerm('');
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchActive = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products?filter=active`);
      const data = await response.json();
      setProducts(data);
      setSearchedProduct(null);
      setSearchTerm('');
    } catch (error) {
      console.error(error);
    }
  };

  const handleFilterChange = (e) => {
    setSearchFilter(e.target.value);
  };

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setProduct((prevState) => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
  
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(product),
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log(data); // Optional: Log the response data for debugging
        // Handle success or display an alert
        fetchProducts(); // Update the product list
        setProduct({
          name: '',
          description: '',
          price: 0,
          imageURL: '',
        }); // Reset the form
        setModalOpen(false); // Close the modal
        Swal.fire('Success', 'Product added successfully', 'success');
      } else {
        throw new Error('Failed to add product');
      }
    } catch (error) {
      console.error('Error:', error);
      Swal.fire('Error', 'Failed to add product', 'error');
    }
  };

  



  const handleRemove = (productId) => {
    Swal.fire({
      title: 'Remove Product',
      text: 'Are you sure you want to remove this product?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Remove',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#6c757d',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const updatedProducts = products.filter((product) => product._id !== productId);
          setProducts(updatedProducts);

          if (searchedProduct && searchedProduct._id === productId) {
            setSearchedProduct(null);
          }

          await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'DELETE',
            headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          });

          Swal.fire('Success', 'Product removed successfully', 'success');
          fetchProducts();
        } catch (error) {
          console.error(error);
        }
      }
    });
  };


  const lastIndex = currentPage * productsPerPage;
  const firstIndex = lastIndex - productsPerPage;
  const displayedProducts = products.slice(firstIndex, lastIndex);
  const totalPages = Math.ceil(products.length / productsPerPage);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <div>
      <div className="search-bar">
        <input
          type="text"
          placeholder="Enter product ID"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          disabled={searchFilter !== 'id'}
        />
        <select value={searchFilter} onChange={handleFilterChange}>
          <option value="all">All</option>
          <option value="id">Search by ID</option>
          <option value="active">Search Active</option>
        </select>
        <button onClick={handleSearch}>
          <FiSearch className="search-icon" />
        </button>
        <button onClick={openModal} className="add-product-button">
          Add Product
        </button>
      </div>

      <div>
        
      <Modal show={modalOpen} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-container">
            <div className="right-column">
              <div className="row">
                <label htmlFor="name">Name:</label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  value={product.name}
                  onChange={handleInputChange}
                  className="product-input"
                  autoComplete="off"
                />
              </div>
              <div className="row">
                <label htmlFor="description">Description:</label>
                <textarea
                  id="description"
                  name="description"
                  value={product.description}
                  onChange={handleInputChange}
                  className="product-input"
                ></textarea>
              </div>
              <div className="row">
                <label htmlFor="price">Price:</label>
                <input
                  type="number"
                  id="price"
                  name="price"
                  value={product.price}
                  onChange={handleInputChange}
                  className="product-input"
                />
              </div>
              <div className="row">
                <label htmlFor="imageURL">Image URL:</label>
                <input
                  type="string"
                  id="imageURL"
                  name="imageURL"
                  value={product.imageURL}
                  onChange={handleInputChange}
                  className="product-input"
                />
              </div>
            </div>
            <div className="bottom-row">
              <button onClick={handleSubmit} type="submit" className="product-button">
                Add Product
              </button>
              <button onClick={closeModal} className="cancel-button">
                Cancel
              </button>
            </div>
          </div>
        </Modal.Body>
        </Modal>
      </div>

      {searchedProduct ? (
        <div>
          <h2>Search Result:</h2>
          <table className="product-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{searchedProduct._id}</td>
                <td>{searchedProduct.name}</td>
                <td>{searchedProduct.description}</td>
                <td>{searchedProduct.price}</td>
                <td>
                  {searchedProduct.isActive ? (
                    <button
                      className="archive-button"
                      onClick={() => handleArchive(searchedProduct._id)}
                    >
                      Archive
                    </button>
                  ) : (
                    <button
                      className="archive-button"
                      onClick={() => handleActivate(searchedProduct._id)}
                    >
                      Activate
                    </button>
                  )}
                  <button
                    className="icon-button"
                    onClick={() => handleEdit(searchedProduct._id)}
                  >
                    <FiEdit className="edit-icon" />
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      ) : (
        <div>
          <table className="product-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {displayedProducts.map((product) => (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>
                    {product.isActive ? (
                      <button
                        className="archive-button"
                        onClick={() => handleArchive(product._id)}
                      >
                        Archive
                      </button>
                    ) : (
                      <button
                        className="archive-button"
                        onClick={() => handleActivate(product._id)}
                      >
                        Activate
                      </button>
                    )}
                    <button
                      className="icon-button"
                      onClick={() => handleEdit(product._id)}
                    >
                      <FiEdit className="edit-icon" />
                    </button>
                    <button
                    className="icon-button"
                    onClick={() => handleRemove(product._id)}
                  >
                    <FiTrash2 className="remove-icon" />
                  </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>

          <div className="pagination">
            {Array.from({ length: totalPages }, (_, index) => (
              <button
                key={index + 1}
                className={currentPage === index + 1 ? 'active' : ''}
                onClick={() => handlePageChange(index + 1)}
              >
                {index + 1}
              </button>
            ))}
          </div>
        </div>
      )}

{editingProduct && (
        <Modal show={showEditModal} onHide={handleCancelEdit}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h2>Edit Product:</h2>
            <div>
              <label>Name:</label>
              <input
                type="text"
                value={editingProduct.name}
                onChange={(e) =>
                  setEditingProduct({ ...editingProduct, name: e.target.value })
                }
              />
            </div>
            <div>
              <label>Description:</label>
              <textarea
                value={editingProduct.description}
                onChange={(e) =>
                  setEditingProduct({
                    ...editingProduct,
                    description: e.target.value,
                  })
                }
              ></textarea>
            </div>
            <div>
              <label>Price:</label>
              <input
                type="number"
                value={editingProduct.price}
                onChange={(e) =>
                  setEditingProduct({ ...editingProduct, price: e.target.value })
                }
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleUpdate} variant="primary">
              Update
            </Button>
            <Button onClick={handleCancelEdit} variant="secondary">
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
}

ProductPage.propTypes = {
  cart: PropTypes.shape({
    _id: PropTypes.string,
    imageURL: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.number,
  }),
};