import { Fragment } from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
import FeaturedProducts from '../pages/FeaturedProducts';
import Hero from '../components/Hero';


export default function Home(){
	return (
		<Fragment>
			<Banner />
			
			

			<section className='featured-products p-5' >
				<div className='container-xxl'>
					<div className='row'>
						<div className='text-center'>
							<h1 className='mb-5'>Featured Products</h1>
						</div>	
					</div>
					<FeaturedProducts />

				</div>
			</section>	
			<Footer />

		</Fragment>
	)
}